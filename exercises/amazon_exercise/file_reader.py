def read_review_file(filename, max_reviews=None):
    """
     This method reads an Amazon review file and stores each review entry into a list of entry dictionaries
    :param filename: path/name of review file
    :param max_reviews: (opitional) only process a given number of reviews
    :return:
    """

    reviews = []
    reviews_file = open(filename, 'r')
    entry = {}
    for line in reviews_file:
        if max_reviews is not None and len(reviews) >= max_reviews:
            break

        colon_index = line.find(':')

        if colon_index == -1:
            reviews.append(entry)
            entry = {}

        else:
            key = line[:colon_index]
            value = line[colon_index + 2:].strip()
            entry[key] = value

    print('Finished reading {} reviews'.format(len(reviews)))
    return reviews
