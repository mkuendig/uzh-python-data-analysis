from statistics import mean, median, mode


def print_my_stats(value_list):
    print(mean(value_list))
    print(median(value_list))
    print(mode(value_list))


def print_review_statistics(reviews):
    """
     Print the mean, media, mode and standard deviation of the review scores and review text lengths
    """
    scores = []
    review_lengths = []

    for review in reviews:
        scores.append(float(review['review/score']))
        review_lengths.append(len(review['review/text'].split(' ')))

    print_my_stats(scores)
    print_my_stats(review_lengths)
