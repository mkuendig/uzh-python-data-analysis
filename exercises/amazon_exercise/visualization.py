import matplotlib
import pygal
from wordcloud import WordCloud

matplotlib.use('TKAgg')
import matplotlib.pyplot as pyplot


def create_wordcloud(word_list, filename):
    """
     Saves image of a wordcloud
     Documentation: https://github.com/amueller/word_cloud
    :param word_list: list of strings
    :param filename: string
    """
    pyplot.clf()
    wordcloud = WordCloud(background_color='white', max_font_size=40, relative_scaling=.5).generate(' '.join(word_list))
    pyplot.imshow(wordcloud)
    pyplot.axis('off')
    pyplot.savefig(filename)


def create_review_length_boxplot(reviews, filename, box_mode='tukey'):
    """
     Creates a boxplot chart with five entries that show the distribution of the review lengths for all possible scores
     Reviews longer than 1500 words are ignored in this example
     Chart documentation: http://www.pygal.org/en/latest/documentation/types/box.html
    :param reviews: list of review dictionaries
    :param filename: string
    :param box_mode: type of boxchart, e.g. None, 'tukey', 'stdv'
    """
    one_star_review_lengths = []
    two_star_review_lengths = []
    three_star_review_lengths = []
    four_star_review_lengths = []
    five_star_review_lengths = []

    for review in reviews:
        review_length = len(review['review/text'].split(' '))
        review_score = int(float(review['review/score']))

        if review_score == 1:
            one_star_review_lengths.append(review_length)
        elif review_score == 2:
            two_star_review_lengths.append(review_length)
        elif review_score == 3:
            three_star_review_lengths.append(review_length)
        elif review_score == 4:
            four_star_review_lengths.append(review_length)
        elif review_score == 5:
            five_star_review_lengths.append(review_length)
        else:
            raise Exception('We hit a unexpected review score: {}'.format(review_score))

    box_plot = pygal.Box(box_mode=box_mode)
    box_plot.title = 'Review length by Start'
    box_plot.add('One', one_star_review_lengths)
    box_plot.add('Two', two_star_review_lengths)
    box_plot.add('Three', three_star_review_lengths)
    box_plot.add('Four', four_star_review_lengths)
    box_plot.add('Five', five_star_review_lengths)
    box_plot.render_to_file(filename)


def create_score_barchart(reviews, filename):
    """
     Creates a barchart that shows the distribution of the scores, i.e. how many 1-star reviews, 2-star reviews, etc.
     Chart documentation: http://www.pygal.org/en/latest/documentation/types/bar.html
    :param reviews: list of review dictionaries
    :param filename: string
    """

    score_buckets = [0, 0, 0, 0, 0]

    for review in reviews:
        score_index = int(float(review['review/score'])) - 1
        score_buckets[score_index] += 1

    bar_chart = pygal.Bar()
    bar_chart.title = 'Frequency of Review Rating'
    bar_chart.x_labels = ['S1', 's2', 's3', 's4', 's5']
    bar_chart.add('Score Review Freq.', score_buckets)
    bar_chart.render_to_file(filename)
