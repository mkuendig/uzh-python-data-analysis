# -*- coding: utf-8 -*-

# Get the total number of countries given by the http://api.population.io/#!/countries
# Use the python "requests" Library

import os.path
import pickle
import requests

countries = requests.get("http://api.population.io:80/1.0/countries")
countries_list = countries.json()["countries"]


def get_clean_country_list():
    pickle_file = 'all_country_list.pkl'

    if os.path.isfile(pickle_file):
        clean_country_list = pickle.load(open(pickle_file, 'rb'))
        return clean_country_list

    clean_country_list = []
    black_list_country = [u'Least developed countries',
                          u'World',
                          u'Less developed regions, excluding least developed countries']
    for country in countries_list:
        if country in black_list_country:
            continue
        response = requests.get(
            "http://api.population.io:80/1.0/life-expectancy/remaining/male/{}/2001-05-11/49y2m/".format(country))
        if response.status_code == 200:
            clean_country_list.append(country)

    pickle.dump(clean_country_list, open(pickle_file, 'wb'))
    return clean_country_list


if __name__ == '__main__':
    clean_country_list = get_clean_country_list()
    testing_countries = clean_country_list[:20]

    print(clean_country_list)
