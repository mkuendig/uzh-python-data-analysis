import csv

output_file = open('movies_information.txt', 'w')
movies_file = open('movies.csv', 'r')
movies = []


def add_empty_lines():
    output_file.write('\n\n######################\n\n')


"""
 Part 1:
 - Create a csv reader and go through each line in the movies.csv
 - For each movie, create a dictionary that contains the following keys:
   'title', 'year', 'length', 'budget', 'rating', 'votes'
 - Append each dictionary to the movies list
"""

movie_reader = csv.reader(movies_file)
# skipping the first line (header file) of the csv
next(movie_reader)

for row in movie_reader:
    movie = {
        'title': row[1],
        'year': int(row[2]),
        'length': int(row[3]) if row[3].isdigit() else -1,
        'budget': int(row[4]) if row[4].isdigit() else -1,
        'rating': float(row[5]) if row[5].isdigit() else -1,
        'votes': int(row[6]) if row[6].isdigit() else -1
    }
    movies.append(movie)

add_empty_lines()

"""
 Part 2:
 Write the number of movies form the movies.csv into the output_file
"""

output_file.write('Number of movies: {}'.format(len(movies)))

add_empty_lines()

"""
 Part 3:
 - Write all movies that begin with "Zero" into the ouput_file
 - Write the number of movies that begin with "Zero" into the output_file
"""
counter = 0
for movie in movies:
    if movie['title'].startswith('Zero '):
        counter += 1
        output_file.write(movie['title'] + '\n')
output_file.write('\n{} movies begin with "Zero"'.format(counter))

add_empty_lines()

"""
 Part 4:
 Write the average score of all movies and the average score of all votes into the output_file
"""

counter = 0
rating_total = 0
for movie in movies:
    rating = movie['rating']
    if rating > 0:
        counter += 1
        rating_total += rating

output_file.write('Average movie rating = {}\n'.format(rating_total / counter))

counter = 0
rating_total = 0
for movie in movies:
    rating = movie['rating']
    votes = movie['votes']
    if rating > 0 and votes > 0:
        counter += votes
        rating_total += rating * votes
output_file.write('Average vote rating = {}'.format(rating_total / counter))

add_empty_lines()

"""
 Part 5:
 Sort the movies list by rating and store it into a sorted_movies list
"""

sorted_movies = sorted(movies, key=lambda movie: movie['rating'], reverse=True)

"""
 Part 6:
 Write the 10 best rated movies with more than 5000 votes into the output_file
"""
output_file.write('Best rated movies with more than 5000 votes:\n\n')
counter = 0
for movie in sorted_movies:
    if counter < 10 and movie['votes'] > 5000:
        counter += 1
        output_file.write('{}: {}\n'.format(movie['title'], movie['rating']))

add_empty_lines()

"""
 Part 7:
 Write the 10 best rated movies with a budget of less than 1,000,000 USD into the output_file
"""
output_file.write('Best rated movies with budget lower than 1,000,000 USD:\n\n')

counter = 0
for movie in sorted_movies:
    if counter < 10 and 1000000 > movie['budget'] > 0:
        counter += 1
        output_file.write('{}: {}\n'.format(movie['title'], movie['rating']))

add_empty_lines()

"""
 Part 8:
 Write the 10 best rated movies with a budget of less than 1,000,000 USD and more than 5000 votes into the output_file
"""
output_file.write('Best rated movies with budget lower than 1,000,000 USD and more than 5000 votes:\n\n')
counter = 0
for movie in sorted_movies:
    if counter < 10 and 1000000 > movie['budget'] > 0 and movie['votes'] > 5000 and movie['rating'] > 0:
        counter += 1
        output_file.write('{}: {}\n'.format(movie['title'], movie['rating']))

output_file.close()
