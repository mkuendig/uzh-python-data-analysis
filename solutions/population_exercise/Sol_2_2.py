# -*- coding: utf-8 -*-

# Get the top 10 countries with the larges population
# given by the function form Sol_2_1
# and http://api.population.io/#!/population/determineTotalPopulationTodayAndTomorrow
# Use the python "requests" Library

import requests
from solutions.population_exercise import Sol_2_1

countries_list = Sol_2_1.get_clean_country_list()


def get_the_top_n(sort_list, n, sort_argument, reverse):
    """Sorts the given list according to the sort argument, return the n top entries in a list

    Keyword arguments:
    sort_list       -- The unsorted list which has to be sorted
    n               -- The number of names in the display
    sort_argument   -- lambda argument with the info what key to sort
    reverse         -- True for descending False for ascending
    """
    sort_list = sorted(sort_list, key=sort_argument, reverse=reverse)
    top_sort_list = []

    counter = 0
    while counter < n:
        top_sort_list.append([sort_list[counter][0], str(sort_list[counter][1])])
        counter += 1

    return top_sort_list


def print_2d_list(print_list):
    """Prints the given 2d list slightly formatted

    Keyword arguments:
    print_list -- the list which has to be printed
    """
    for item in print_list:
        print(' - '.join(item))


list_to_sort = []
# containing sublists [['Guinea-Bissau', 1693398], ['Switzerland', 8211700]...]

# iterating over countries and determine the population numbers
for country in countries_list:
    print("Processing: " + country)
    population = requests.get("http://api.population.io:80/1.0/population/" + country + "/today-and-tomorrow/")
    total_population = population.json()["total_population"]

    country_list = [country, total_population[0]["population"]]
    list_to_sort.append(country_list)

# calling the sorting function
key = (lambda land: land[1])
top_list = get_the_top_n(list_to_sort, 10, key, True)

# calling the printing function
print_2d_list(top_list)